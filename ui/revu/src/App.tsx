import * as React from 'react';
import { HashRouter, Route, Switch } from "react-router-dom"
import './App.css';
import FileUploadPage from './components/pages/fileUploadPage';
import HomePage from './components/pages/homePage';

class App extends React.Component {
  public render() {
    return (
      <HashRouter>
        <div className="main">
        <Switch>
          <Route exact={true} path="/" component={HomePage} />
          <Route path="/upload" component={FileUploadPage} />
        </Switch>
        </div>
      </HashRouter>
    );
  }
}

export default App;
