import * as React from "react"
import { FaUpload } from "react-icons/fa"
import { NavLink } from 'react-router-dom';
import PageTemplate from '../pageTemplate'

class HomePage extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        return (
            <PageTemplate>
                Start with uploading a file <NavLink to="/upload"><FaUpload/></NavLink>
            </PageTemplate>
        )
    }
}

export default HomePage