import * as React from "react"
import { FaHome as HomeIcon } from "react-icons/fa"
import { NavLink } from "react-router-dom"

const selectedStyle = {
    backgroundColor: "steelblue",
    color: "white"
}

export const MainMenu = () => {
    return(
        <nav>
            <div className="main-menu">
                <NavLink to="/" exact={true} activeStyle={selectedStyle}>
                    <div className="main-menu-item">
                        <HomeIcon/>
                    </div>
                </NavLink>
                <NavLink to="/upload" activeStyle={selectedStyle}>
                    <div className="main-menu-item">
                        Upload
                    </div>
                </NavLink>
            </div>
        </nav>
    )
}