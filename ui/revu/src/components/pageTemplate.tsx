import * as React from "react"
import { MainMenu } from "./menus/menus"

class PageTemplate extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        const children = this.props.children
        return (
          <div className="page">
            <div className="menu">
                <MainMenu />
            </div>
            <div className="content">
                {children}
            </div>
          </div>
        );
      }

}

export default PageTemplate
