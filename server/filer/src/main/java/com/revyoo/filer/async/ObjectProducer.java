package com.revyoo.filer.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class ObjectProducer {

    private static final Logger logger = LoggerFactory.getLogger(ObjectProducer.class);
    private static final String TOPIC = "objects";

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public ObjectProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void publishObjectEvent(String objectType, String objectString) {
        logger.info("-> Publishing new object: Type {} | Object {}", objectType, objectString);
        kafkaTemplate.send(TOPIC, objectType + "::" + objectString);
    }
}
