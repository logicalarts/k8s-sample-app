package com.revyoo.filer.resource;

import com.revyoo.platform.metrics.resource.BaseResourceConfig;
import io.opentracing.Tracer;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Configuration
@Component
public class ApplicationConfig extends BaseResourceConfig {

    @Inject
    public ApplicationConfig(Tracer tracer){
        super(tracer);
        packages("com.revyoo.filer");
        // Register the Feature for Multipart Uploads (File Upload):
        register(MultiPartFeature.class);
    }

}
