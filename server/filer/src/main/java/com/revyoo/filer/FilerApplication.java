package com.revyoo.filer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.revyoo")
public class FilerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilerApplication.class, args);
	}

}
