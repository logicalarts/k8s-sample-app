package com.revyoo.filer.resource;

import com.mysql.cj.xdevapi.*;
import com.revyoo.filer.config.DBConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/download")
public class FileDownloadResource {

    private Logger logger = LoggerFactory.getLogger(FileUploadResource.class);
    private DBConfig dbConfig;

    @Inject
    public FileDownloadResource(DBConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{objectType}")
    public Response getObjects(@PathParam("objectType") String objectType) {
        String connectionURL = dbConfig.getConnectionURL();
        logger.info("DB Connection URL: {}", connectionURL);
        SessionFactory sessionFactory = new SessionFactory();
        Session session = sessionFactory.getSession(connectionURL);
        Schema schema = session.getSchema(dbConfig.getSchema());
        Collection collection = schema.getCollection(objectType, true);
        List<DbDoc> objects = collection.find().execute().fetchAll();
        return Response.ok(objects.toString()).build();
    }

}
