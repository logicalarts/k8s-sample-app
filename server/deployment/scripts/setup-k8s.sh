#!/bin/bash
#Find if service is already running
set -x

MINIKUBE_STATUS=$(minikube status | grep Stopped )
if [[ $MINIKUBE_STATUS = *"Stopped"* ]] ; then
    minikube start
fi
eval $(minikube docker-env)

#Delete current deployments if any
./delete-deployments.sh

#Create new deployments
./create-deployments.sh

set +x
