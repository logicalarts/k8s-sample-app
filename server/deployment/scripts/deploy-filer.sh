#!/bin/bash
set -x
#Set docker env to minikube
eval $(minikube docker-env)

#Delete existing service and pod
kubectl delete service revyoo-filer
kubectl delete deployment revyoo-filer

#Build app and image
pushd .;
cd ../../
pwd
./gradlew filer:build filer:jibDockerBuild
popd;

#Deploy pod and service
kubectl create -f ../k8s/filer-deployment.yml
kubectl create -f ../k8s/filer-service.yml

set +x