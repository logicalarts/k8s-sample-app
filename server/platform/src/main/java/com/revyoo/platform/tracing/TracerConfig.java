package com.revyoo.platform.tracing;

import io.opentracing.contrib.jaxrs2.server.SpanFinishingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;

@Configuration
public class TracerConfig {

    //This filter is required to close the root spans created by OpenTracing JAX-RS instrumentation.
    @Bean
    public FilterRegistrationBean<SpanFinishingFilter> spanFinishingFilter() {
        FilterRegistrationBean<SpanFinishingFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new SpanFinishingFilter());
        filterRegistrationBean.setAsyncSupported(true);
        filterRegistrationBean.setDispatcherTypes(DispatcherType.REQUEST);
        filterRegistrationBean.addUrlPatterns("*");
        return filterRegistrationBean;
    }
}
