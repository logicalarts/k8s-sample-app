package com.revyoo.platform.metrics.provider;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.inject.Singleton;

@Configuration
@Service
@Singleton
public class PrometheusMetricsProvider implements MetricsProvider {

    private Logger logger = LoggerFactory.getLogger(MetricsProvider.class);
    private PrometheusMeterRegistry prometheusRegistry;

    public PrometheusMetricsProvider() {
        prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
    }

    public MeterRegistry getMeterRegistry() {
        return prometheusRegistry;
    }

    public String scrape() {
        return prometheusRegistry.scrape();
    }

}
