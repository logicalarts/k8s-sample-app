package com.revyoo.platform.metrics.provider;

import io.micrometer.core.instrument.MeterRegistry;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface MetricsProvider {

    MeterRegistry getMeterRegistry();

    String scrape();

}
