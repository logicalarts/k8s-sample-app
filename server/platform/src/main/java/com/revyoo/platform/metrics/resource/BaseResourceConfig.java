package com.revyoo.platform.metrics.resource;

import io.opentracing.Tracer;
import io.opentracing.contrib.jaxrs2.server.ServerTracingDynamicFeature;
import org.glassfish.jersey.server.ResourceConfig;

public abstract class BaseResourceConfig extends ResourceConfig {

    public BaseResourceConfig(Tracer tracer) {
        packages("com.revyoo.platform");
        register(new ServerTracingDynamicFeature.Builder(tracer).build());
    }

}
