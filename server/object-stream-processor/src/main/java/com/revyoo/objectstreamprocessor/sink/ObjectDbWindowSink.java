package com.revyoo.objectstreamprocessor.sink;

import com.mysql.cj.xdevapi.*;
import com.revyoo.objectstreamprocessor.config.DBConfig;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;

public class ObjectDbWindowSink implements WindowFunction<String, String, String, TimeWindow> {

    private static final Logger logger = LoggerFactory.getLogger(ObjectDbWindowSink.class);

    private DBConfig dbConfig;

    public ObjectDbWindowSink(DBConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    @Override
    public void apply(String objectType, TimeWindow window, Iterable<String> input, Collector<String> out) throws Exception {
        logger.info("--> Consuming object in Flink job : {}", input);
        String connectionURL = dbConfig.getConnectionURL();
        logger.info("DB Connection URL: {}", connectionURL);
        SessionFactory sessionFactory = new SessionFactory();
        Session session = sessionFactory.getSession(connectionURL);
        Schema schema = session.getSchema(dbConfig.getSchema());
        Collection collection = schema.createCollection(objectType, true);
        for (String objectData : input) {
            String typeSeparator = "::";
            String objectString = objectData.replaceFirst(objectType + typeSeparator, "");
            try {
                DbDoc dbDoc = JsonParser.parseDoc(new StringReader(objectString));
                collection.add(dbDoc).execute();
            } catch (IOException e) {
                logger.debug("Unable to store object in database: {}", e.getMessage());
                throw e;
            }
        }
        session.commit();
        session.close();
        logger.debug("--> Created collection for: {}", objectType);
    }

}