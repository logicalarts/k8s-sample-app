package com.revyoo.objectstreamprocessor.sink;

import org.apache.flink.api.java.functions.KeySelector;

public class ObjectTypeKeySelector implements KeySelector<String, String> {

    @Override
    public String getKey(String value) throws Exception {
        String typeSeparator = "::";
        return value.split(typeSeparator)[0];
    }
}
