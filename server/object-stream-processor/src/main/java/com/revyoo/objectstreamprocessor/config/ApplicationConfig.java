package com.revyoo.objectstreamprocessor.config;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.io.Serializable;

public class ApplicationConfig implements Serializable {

    private DBConfig database;
    private KafkaConfig kafka;

    public DBConfig getDatabase() {
        return database;
    }

    public void setDatabase(DBConfig database) {
        this.database = database;
    }

    public KafkaConfig getKafka() {
        return kafka;
    }

    public void setKafka(KafkaConfig kafka) {
        this.kafka = kafka;
    }

    public static ApplicationConfig getInstance() {
        Yaml yaml = new Yaml(new Constructor(ApplicationConfig.class));
        InputStream inputStream = ApplicationConfig.class.getClassLoader().getResourceAsStream("application.yml");
        return yaml.load(inputStream);
    }
}
