package com.revyoo.objectstreamprocessor.config;

import java.io.Serializable;
import java.util.Optional;

public class KafkaConfig implements Serializable {

    private String bootstrapServerHost;
    private String bootstrapServerPort;

    public String getBootstrapServerHost() {
        return Optional.ofNullable(System.getenv(bootstrapServerHost)).orElse("localhost");
    }

    public void setBootstrapServerHost(String bootstrapServerHost) {
        this.bootstrapServerHost = bootstrapServerHost;
    }

    public String getBootstrapServerPort() {
        return Optional.ofNullable(System.getenv(bootstrapServerPort)).orElse("9092");
    }

    public void setBootstrapServerPort(String bootstrapServerPort) {
        this.bootstrapServerPort = bootstrapServerPort;
    }

    @Override
    public String toString() {
        return "KafkaConfig{" +
                "bootstrapServerHost='" + getBootstrapServerHost() + '\'' +
                ", bootstrapServerPort='" + getBootstrapServerPort() + '\'' +
                '}';
    }
}
