package com.revyoo.objectstreamprocessor.config;

import java.io.Serializable;
import java.util.Optional;

public class DBConfig implements Serializable {

    private String host;
    private String port;
    private String schema;
    private String username;
    private String password;

    public String getHost() {
        return Optional.ofNullable(System.getenv(host)).orElse("localhost");
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return Optional.ofNullable(System.getenv(port)).orElse("33060");
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getConnectionURL() {
        return String.format("mysqlx://%1s:%2s/%3s?user=%4s&password=%5s",
                getHost(), getPort(), getSchema(), getUsername(), getPassword());
    }

    @Override
    public String toString() {
        return "DBConfig{" +
                "host='" + getHost() + '\'' +
                ", port='" + getPort() + '\'' +
                ", schema='" + schema + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
