package com.revyoo.objectstreamprocessor;

import com.revyoo.objectstreamprocessor.config.ApplicationConfig;
import com.revyoo.objectstreamprocessor.config.DBConfig;
import com.revyoo.objectstreamprocessor.config.KafkaConfig;
import com.revyoo.objectstreamprocessor.sink.ObjectDbWindowSink;
import com.revyoo.objectstreamprocessor.sink.ObjectTypeKeySelector;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ObjectStreamProcessorApplication {

    private static Logger logger = LoggerFactory.getLogger(ObjectStreamProcessorApplication.class);

    private static FlinkKafkaConsumer<String> createObjectConsumer(KafkaConfig kafkaConfig) {
        Properties properties = new Properties();
        String kafkaHost = kafkaConfig.getBootstrapServerHost();
        String kafkaPort = kafkaConfig.getBootstrapServerPort();
        properties.setProperty("bootstrap.servers", String.format("%1s:%2s", kafkaHost, kafkaPort));
        properties.setProperty("group.id", "flink_group");
        return new FlinkKafkaConsumer<>("objects", new SimpleStringSchema(), properties);
    }

    public static void main(String[] args) {
        ApplicationConfig appConfig = ApplicationConfig.getInstance();
        DBConfig dbConfig = appConfig.getDatabase();
        KafkaConfig kafkaConfig = appConfig.getKafka();
        logger.debug("--> DB Config: {}", dbConfig);
        logger.debug("--> Kafka Config: {}", kafkaConfig);
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<String> input = environment.addSource(createObjectConsumer(kafkaConfig));
        input.keyBy(new ObjectTypeKeySelector())
                .timeWindow(Time.milliseconds(500))
                .apply(new ObjectDbWindowSink(dbConfig));
        try {
            environment.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
