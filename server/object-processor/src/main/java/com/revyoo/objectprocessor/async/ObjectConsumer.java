package com.revyoo.objectprocessor.async;

import com.mysql.cj.xdevapi.*;
import com.revyoo.objectprocessor.config.DBConfig;
import com.revyoo.platform.metrics.provider.MetricsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;

@Component
public class ObjectConsumer {

    private static final Logger logger = LoggerFactory.getLogger(ObjectConsumer.class);
    private static final String TOPIC = "objects";

    private DBConfig dbConfig;
    private MetricsProvider metricsProvider;

    @Autowired
    public ObjectConsumer(DBConfig dbConfig, MetricsProvider metricsProvider) {
        this.dbConfig = dbConfig;
        this.metricsProvider = metricsProvider;
    }

    @KafkaListener(topics = TOPIC, groupId = "group_id")
    public void consumeObject(String objectData) {
        String typeSeparator = "::";
        String objectType = objectData.split(typeSeparator)[0];
        String objectString = objectData.replaceFirst(objectType + typeSeparator, "");
        logger.info("--> Consuming object : {}", objectString);
        String connectionURL = dbConfig.getConnectionURL();
        logger.info("DB Connection URL: {}", connectionURL);
        SessionFactory sessionFactory = new SessionFactory();
        Session session = sessionFactory.getSession(connectionURL);
        Schema schema = session.getSchema(dbConfig.getSchema());
        Collection collection = schema.createCollection(objectType, true);
        try {
            DbDoc dbDoc = JsonParser.parseDoc(new StringReader(objectString));
            collection.add(dbDoc).execute();
            metricsProvider.getMeterRegistry().counter("object_events_processed").increment();
        } catch (IOException e) {
            logger.debug("Unable to store object in database: {}", e.getMessage());
        }
        session.commit();
        session.close();
        logger.debug("--> Created collection for: {}", objectType);
    }

}
