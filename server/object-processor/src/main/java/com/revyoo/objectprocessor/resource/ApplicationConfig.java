package com.revyoo.objectprocessor.resource;

import com.revyoo.platform.metrics.resource.BaseResourceConfig;
import io.opentracing.Tracer;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;

@Configuration
public class ApplicationConfig extends BaseResourceConfig {

    @Inject
    public ApplicationConfig(Tracer tracer){
        super(tracer);
        packages("com.revyoo.objectprocessor");
    }

}
