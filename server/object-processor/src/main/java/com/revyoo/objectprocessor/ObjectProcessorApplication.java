package com.revyoo.objectprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.revyoo")
public class ObjectProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObjectProcessorApplication.class, args);
    }

}
